from django.db import models

#create your models here
class Projects(models.Model):
    project_name = models.CharField(max_length=200)
    percentage = models.IntegerField(default=0)

    def __str__(self):
        return self.project_name
